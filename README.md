# README #

This repository contains scripts for installing, starting and stopping a group of agents. This is particularly useful when running experiments.

## Instructions ##

### Create a symbolic link to your agent config directory ###

The scripts need to know where to look for the agent configuration files. To make running different experiments easier, we map a symbolic link to the directory containing the experiment-specific configurations.

`$ ln -sfn [path to directory containing the agent configuration files] config`

This creates a symbolic link called "config" that will point to the directory you specify.

### Install and run agents ###

The make-agent-group.sh script installs and runs a group of agents specified in a file.

`$ . make-agent-group.sh [path to volttron source] [path to agent list]`

The volttron source directory is the directory where the volttron repo was cloned. It contains a directory called "scripts" that perform the agent installation. In our deployments, this is usually "/home/volttron/volttron"

See below for the format of the agent list.

### Stop agents ###

The stop-agent-group.sh script stops a group of agents specified in a file.

`$ . stop-agent-group.sh [path to agent list]`

### Start agents already installed ###

`$ . start-agent-group.sh [path to agent list]`

## Agent list format ##

The start-agent-group.sh script starts a group of agents specified in a file. The agents must already be installed.

The agent list is a tab-separated-value (TSV) formatted text file. Each line has the following elements:

`[path to agent source]	[path to agent config]	[tag for the agent]`

Note that relative paths in the agent list will be interpreted relative to the directory where the make-agent-group.sh script is run. The path to the agent config ideally uses the symbolic link we created above. This allows us to install, start and stop the same group of agents with different configuration files using a single agent list. Typically one would use the same agent list for start-agent-group.sh and stop-agent-group.sh; they expect the same format.